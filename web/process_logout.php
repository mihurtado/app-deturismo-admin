<?php

require 'includes/auth_load.php';

if (!$auth->isLogged()) {
  header("Location: index.php");
  exit();
}

$session_hash = $auth->getSessionHash();

$auth->logout($session_hash);
header("Location: index.php");
