<?php

if (isset($_POST['email']) && isset($_POST['password'])) {
  $email = $_POST['email'];
  $password = $_POST['password'];

  $remember = false;
  if (isset($_POST['remember']) && $_POST['remember'] == "1") {
    $remember = true;
  }

  require 'includes/auth_load.php';

  $login = $auth->login($email, $password, $remember);

  if (!$login['error']) {
    // Set cookie
    setcookie('authID', $login['hash'], $login['expire'], '/');
    header("Location: index.php");
  }
  else
  {
    header("Location: index.php?m=login&e=" . $login['message']);
  }
}
else
{
  $error = 'No email or password provided';
  header("Location: index.php?m=login&e=" . $error);
}