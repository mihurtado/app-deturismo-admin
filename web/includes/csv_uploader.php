<a href="#csv-upload" class="btn btn-primary" data-toggle="collapse">Subir CSV</a>

<div id="csv-upload" class="collapse">
  <p>Subir archivo CSV con precios a actualizar</p>
  <form class="form-inline" action="?m=autos" method="post" enctype="multipart/form-data">
    <div class="form-group">
      <input type="file" name="file" required>
    </div>
    <input type="submit" class="btn btn-primary" name="submit" value="Subir">
  </form>
</div>