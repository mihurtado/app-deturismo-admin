<?php

function hash_password($postdata, $primary, $xcrud){
  $password = password_hash($postdata->get('password'), PASSWORD_BCRYPT, ['cost' => 10]);
  $postdata->set('password', $password);
}
