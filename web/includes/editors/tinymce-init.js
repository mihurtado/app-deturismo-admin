tinyMCE.init({
	mode: "textareas", // important
	editor_selector: "editor-instance",	// important
	valid_elements: '*[*]',
  height: "250",
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code jbimages'
  ],
  toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages',
  relative_urls: false
});