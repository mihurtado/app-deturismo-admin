<?php

  require 'includes/auth_load.php';

    if (!$auth->isLogged()) {
        header("Location: ?m=login");
        exit();
    }
    $session_hash = $auth->getSessionHash();
    $user_id = $auth->getSessionUID($session_hash);
    $user_data = $auth->getUser($user_id);
    $user = (object) array('logged' => true, 'hash' => $session_hash, 'email' => $user_data['email'], 'admin_level' => $user_data['admin_level']);
    

?>