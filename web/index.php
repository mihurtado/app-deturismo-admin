<?php

// Get module
if(!isset($_GET['m'])) {
  $module = 'home';
} else {
  $module = $_GET['m'];
}

if ($module != 'login') {
  require 'includes/checkLogin.php';
}

require ('includes/xcrud/xcrud.php');
// Set lang
$xcrud = Xcrud::get_instance();
//$xcrud->language($language); // will load en.ini lang file from languages folder
$xcrud->language('es') // Always espanol

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="De Turismo Administrador">
    <meta name="author" content="Infinite.cl">

    <title>De Turismo Administrador</title>

    <!-- Bootstrap core CSS -->
    <?php echo Xcrud::load_css() ?>

    <!-- Custom styles for this template -->
    <link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">De Turismo Administrador</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="?m=categories">Categorias</a></li>
            <li><a href="?m=publications">Publicaciones</a></li>
            <li><a href="?m=events">Eventos</a></li>
            <li><a href="?m=promotions">Promociones</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <?php if (isset($user) && $user->logged): ?>
            <li><a href="?m=user"><?php echo $user->email; ?></a></li>
            <li><a href="process_logout.php">Log Out</a></li>
            <?php else: ?>
            <li><a href="?m=login">Log In</a></li>
            <?php endif; ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">

      <?php 
        include('modules/' . $module . '.php');
      ?>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <?php
    echo Xcrud::load_js();
    ?>
  </body>
</html>
