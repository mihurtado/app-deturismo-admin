<?php

$xcrud->table('categories');
$xcrud->columns('id, menu_icon, map_icon, parent, name, orden');

$xcrud->relation('parent','categories','id','name');

$xcrud->no_editor('name,description');

// Image uploader
$xcrud->change_type('map_icon', 'image', '', array('path' => '../../../app_assets/images/map_icons/'));
$xcrud->change_type('menu_icon', 'image', '', array('path' => '../../../app_assets/images/guide_icons/'));

echo $xcrud->render();