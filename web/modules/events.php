<?php

$xcrud->table('events');
$xcrud->columns('id,name,image_url');

$xcrud->fields('name,description,image_url', false, 'Information');
$xcrud->fields('address,latitude,longitude', false, 'Location');


$xcrud->no_editor('name,address');

$xcrud->change_type('image_url', 'image', '', array('manual_crop' => false, 'path' => '../../../app_assets/images/events/'));

// Category relation
$xcrud->relation('category_id','categories','id','name','parent is not null');


echo $xcrud->render();