<?php

$xcrud->table('publications');
$xcrud->columns('id,name,category_id');

$xcrud->fields('category_id,id,name,description,equipment', false, 'Information');
$xcrud->fields('email,url,phone,cellphone', false, 'Contact');
$xcrud->fields('address,latitude,longitude', false, 'Location');

$xcrud->no_editor('name,description,equipment,email,url,phone,cellphone,address');

// Category relation
$xcrud->relation('category_id','categories','id','name','parent is not null');

$images = $xcrud->nested_table('Images', 'id', 'publication_images', 'publication_id');
$images->columns('id,url,description, is_main');
$images->fields('url,description,is_main');
$images->no_editor('description');
$images->change_type('url', 'image', '', array('path' => '../../../app_assets/images/publications/'));




echo $xcrud->render();