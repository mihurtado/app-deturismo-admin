<?php

$xcrud->table('publication_images');
$xcrud->columns('publication_id,id,url,description');

// Category relation
$xcrud->relation('publication_id','publications','id','name');

$xcrud->no_editor('description');

$xcrud->change_type('url', 'image', '', array('manual_crop' => false, 'path' => '../../../app_assets/images/publications/'));

echo $xcrud->render();