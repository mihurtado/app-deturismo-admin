<?php

$xcrud->table('promotions');

$xcrud->columns('id,start,end,image,url');

$xcrud->fk_relation('Categories to Show','id','promotion_shows','promotion_id','location_id','categories','id','name', 'parent is null');

$xcrud->fields('start,end,image,url,text,show_popup', false, 'Information');
$xcrud->fields('Categories to Show', false, 'Where to show');


$xcrud->change_type('image', 'image', '', array('manual_crop' => false, 'path' => '../../../app_assets/images/promotions/'));
$xcrud->change_type('start','timestamp');
$xcrud->change_type('end','timestamp');

$xcrud->no_editor('url,text');


echo $xcrud->render();