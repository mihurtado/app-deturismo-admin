
--
-- Base de datos: `deturism_app_algarrobo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` varchar(50) NOT NULL,
  `parent` varchar(50) DEFAULT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `menu_icon` text,
  `map_icon` text,
  PRIMARY KEY (`id`)
);

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `parent`, `name`, `description`, `menu_icon`, `map_icon`) VALUES
('prueba', NULL, 'Prueba', 'Prueba', 'donde-alojar-1.png', NULL),
('donde-comprar', NULL, 'De Compras', 'De Compras', '2yikrzm1dkcgc44808.png', 'mr5ye3f80wg8k8cgkc.png'),
('alojamiento', NULL, 'Alojamiento', '', '2ho9iahk2e2o4o0s88.png', 'pye2ivw5ffkk44oo8k.png'),
('apart-hotel', 'alojamiento', 'Apart Hotel', '', NULL, NULL),
('cabanas', 'alojamiento', 'Cabañas', '', NULL, NULL),
('hoteles', 'alojamiento', 'Hoteles', '', NULL, NULL),
('bed-and-breakfast', 'alojamiento', 'Bed & Breakfast', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promotions`
--

CREATE TABLE IF NOT EXISTS `promotions` (
  `id` int(11) NOT NULL ,
  `start` int(11) NOT NULL,
  `end` int(11) NOT NULL,
  `image` text NOT NULL,
  `text` text NOT NULL,
  `url` text NOT NULL,
  `show_popup` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

--
-- Volcado de datos para la tabla `promotions`
--

INSERT INTO `promotions` (`id`, `start`, `end`, `image`, `text`, `url`, `show_popup`) VALUES
(1, 1502128800, 1504116000, 'wyvds85915c8wos0g4.png', 'Prueba', 'http://emol.com', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promotion_shows`
--

CREATE TABLE IF NOT EXISTS `promotion_shows` (
  `promotion_id` int(11) NOT NULL,
  `location_id` varchar(50) NOT NULL,
  PRIMARY KEY (`promotion_id`,`location_id`)
);

--
-- Volcado de datos para la tabla `promotion_shows`
--

INSERT INTO `promotion_shows` (`promotion_id`, `location_id`) VALUES
(1, 'alojamiento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publications`
--

CREATE TABLE IF NOT EXISTS `publications` (
  `id` varchar(50) NOT NULL,
  `category_id` varchar(50) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `equipment` text,
  `email` text,
  `url` text,
  `phone` text,
  `cellphone` text,
  `address` text,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `votes_number` bigint(20) NOT NULL DEFAULT '0',
  `votes_avg` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

--
-- Volcado de datos para la tabla `publications`
--

INSERT INTO `publications` (`id`, `category_id`, `name`, `description`, `equipment`, `email`, `url`, `phone`, `cellphone`, `address`, `latitude`, `longitude`, `votes_number`, `votes_avg`) VALUES
('donde-alojar-1', 'apart-hotel', 'Cabañas Toconao', '<p>Caba&ntilde;as para 2 personas de dise&ntilde;o muy acogedor y hermoso entorno natural. Ubicadas a 1.5 km. de las playas de Algarrobo, en un lugar privilegiado, nuestras caba&ntilde;as son ideales para escaparse del ruido de la ciudad y relajarse en un entorno rodeado por la naturaleza.</p>', '<p>Completamente equipadas con calefacci&oacute;n a le&ntilde;a, asadera y estacionamiento propio, sin televisor ni internet. Se incluye canasta con snack de bienvenida.</p>', 'contreras.ossa@cabanastoconao.cl', 'http://www.cabanastoconao.cl/', NULL, '9 9277 4018', 'Condominio Santa Genoveva Parcela B4', -33.3716, -71.6464, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publication_images`
--

CREATE TABLE IF NOT EXISTS `publication_images` (
  `publication_id` varchar(50) NOT NULL,
  `id` int(11) NOT NULL ,
  `url` text NOT NULL,
  `description` text NOT NULL,
  `is_main` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`publication_id`,`id`)
);

--
-- Volcado de datos para la tabla `publication_images`
--

INSERT INTO `publication_images` (`publication_id`, `id`, `url`, `description`, `is_main`) VALUES
('donde-alojar-1', 1, 'fm481y63jz4k8g88.jpg', '', 1),
('donde-alojar-1', 2, '1wnsvnit803ocgs4w0.jpg', '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
